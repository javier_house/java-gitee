
# PullRequestMergePutParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | 用户授权码 |  [optional]
**mergeMethod** | [**MergeMethodEnum**](#MergeMethodEnum) | 可选。合并PR的方法，merge（合并所有提交）和 squash（扁平化分支合并）。默认为merge。 |  [optional]
**pruneSourceBranch** | **Boolean** | 可选。合并PR后是否删除源分支，默认false（不删除） |  [optional]
**title** | **String** | 可选。合并标题，默认为PR的标题 |  [optional]
**description** | **String** | 可选。合并描述，默认为 \&quot;Merge pull request !{pr_id} from {author}/{source_branch}\&quot;，与页面显示的默认一致。 |  [optional]


<a name="MergeMethodEnum"></a>
## Enum: MergeMethodEnum
Name | Value
---- | -----
MERGE | &quot;merge&quot;
SQUASH | &quot;squash&quot;



