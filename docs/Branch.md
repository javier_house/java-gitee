
# Branch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**commit** | **String** |  |  [optional]
**_protected** | **String** |  |  [optional]
**protectionUrl** | **String** |  |  [optional]



