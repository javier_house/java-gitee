
# Issue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**url** | **String** |  |  [optional]
**repositoryUrl** | **String** |  |  [optional]
**labelsUrl** | **String** |  |  [optional]
**commentsUrl** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**parentUrl** | **String** |  |  [optional]
**number** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**body** | **String** |  |  [optional]
**bodyHtml** | **String** |  |  [optional]
**user** | [**UserBasic**](UserBasic.md) |  |  [optional]
**labels** | [**List&lt;Label&gt;**](Label.md) |  |  [optional]
**assignee** | [**UserBasic**](UserBasic.md) |  |  [optional]
**collaborators** | [**List&lt;UserBasic&gt;**](UserBasic.md) |  |  [optional]
**repository** | **String** |  |  [optional]
**milestone** | [**Milestone**](Milestone.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**planStartedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**deadline** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**finishedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**scheduledTime** | **String** |  |  [optional]
**comments** | **Integer** |  |  [optional]
**issueType** | **String** |  |  [optional]
**program** | [**ProgramBasic**](ProgramBasic.md) |  |  [optional]



