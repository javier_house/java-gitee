
# Release

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**tagName** | **String** |  |  [optional]
**targetCommitish** | **String** |  |  [optional]
**prerelease** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**body** | **String** |  |  [optional]
**author** | **String** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**assets** | **String** |  |  [optional]



