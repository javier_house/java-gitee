# WebhooksApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoHooksId**](WebhooksApi.md#deleteV5ReposOwnerRepoHooksId) | **DELETE** /v5/repos/{owner}/{repo}/hooks/{id} | 删除一个仓库WebHook
[**getV5ReposOwnerRepoHooks**](WebhooksApi.md#getV5ReposOwnerRepoHooks) | **GET** /v5/repos/{owner}/{repo}/hooks | 列出仓库的WebHooks
[**getV5ReposOwnerRepoHooksId**](WebhooksApi.md#getV5ReposOwnerRepoHooksId) | **GET** /v5/repos/{owner}/{repo}/hooks/{id} | 获取仓库单个WebHook
[**patchV5ReposOwnerRepoHooksId**](WebhooksApi.md#patchV5ReposOwnerRepoHooksId) | **PATCH** /v5/repos/{owner}/{repo}/hooks/{id} | 更新一个仓库WebHook
[**postV5ReposOwnerRepoHooks**](WebhooksApi.md#postV5ReposOwnerRepoHooks) | **POST** /v5/repos/{owner}/{repo}/hooks | 创建一个仓库WebHook
[**postV5ReposOwnerRepoHooksIdTests**](WebhooksApi.md#postV5ReposOwnerRepoHooksIdTests) | **POST** /v5/repos/{owner}/{repo}/hooks/{id}/tests | 测试WebHook是否发送成功


<a name="deleteV5ReposOwnerRepoHooksId"></a>
# **deleteV5ReposOwnerRepoHooksId**
> deleteV5ReposOwnerRepoHooksId(owner, repo, id, accessToken)

删除一个仓库WebHook

删除一个仓库WebHook

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.WebhooksApi;


WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | Webhook的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoHooksId(owner, repo, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#deleteV5ReposOwnerRepoHooksId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| Webhook的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoHooks"></a>
# **getV5ReposOwnerRepoHooks**
> List&lt;Hook&gt; getV5ReposOwnerRepoHooks(owner, repo, accessToken, page, perPage)

列出仓库的WebHooks

列出仓库的WebHooks

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.WebhooksApi;


WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Hook> result = apiInstance.getV5ReposOwnerRepoHooks(owner, repo, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#getV5ReposOwnerRepoHooks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Hook&gt;**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoHooksId"></a>
# **getV5ReposOwnerRepoHooksId**
> Hook getV5ReposOwnerRepoHooksId(owner, repo, id, accessToken)

获取仓库单个WebHook

获取仓库单个WebHook

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.WebhooksApi;


WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | Webhook的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Hook result = apiInstance.getV5ReposOwnerRepoHooksId(owner, repo, id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#getV5ReposOwnerRepoHooksId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| Webhook的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoHooksId"></a>
# **patchV5ReposOwnerRepoHooksId**
> Hook patchV5ReposOwnerRepoHooksId(owner, repo, id, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents)

更新一个仓库WebHook

更新一个仓库WebHook

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.WebhooksApi;


WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | Webhook的ID
String url = "url_example"; // String | 远程HTTP URL
String accessToken = "accessToken_example"; // String | 用户授权码
String password = "password_example"; // String | 请求URL时会带上该密码，防止URL被恶意请求
Boolean pushEvents = true; // Boolean | Push代码到仓库
Boolean tagPushEvents = true; // Boolean | 提交Tag到仓库
Boolean issuesEvents = true; // Boolean | 创建/关闭Issue
Boolean noteEvents = true; // Boolean | 评论了Issue/代码等等
Boolean mergeRequestsEvents = true; // Boolean | 合并请求和合并后
try {
    Hook result = apiInstance.patchV5ReposOwnerRepoHooksId(owner, repo, id, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#patchV5ReposOwnerRepoHooksId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| Webhook的ID |
 **url** | **String**| 远程HTTP URL |
 **accessToken** | **String**| 用户授权码 | [optional]
 **password** | **String**| 请求URL时会带上该密码，防止URL被恶意请求 | [optional]
 **pushEvents** | **Boolean**| Push代码到仓库 | [optional] [default to true]
 **tagPushEvents** | **Boolean**| 提交Tag到仓库 | [optional]
 **issuesEvents** | **Boolean**| 创建/关闭Issue | [optional]
 **noteEvents** | **Boolean**| 评论了Issue/代码等等 | [optional]
 **mergeRequestsEvents** | **Boolean**| 合并请求和合并后 | [optional]

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoHooks"></a>
# **postV5ReposOwnerRepoHooks**
> Hook postV5ReposOwnerRepoHooks(owner, repo, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents)

创建一个仓库WebHook

创建一个仓库WebHook

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.WebhooksApi;


WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String url = "url_example"; // String | 远程HTTP URL
String accessToken = "accessToken_example"; // String | 用户授权码
String password = "password_example"; // String | 请求URL时会带上该密码，防止URL被恶意请求
Boolean pushEvents = true; // Boolean | Push代码到仓库
Boolean tagPushEvents = true; // Boolean | 提交Tag到仓库
Boolean issuesEvents = true; // Boolean | 创建/关闭Issue
Boolean noteEvents = true; // Boolean | 评论了Issue/代码等等
Boolean mergeRequestsEvents = true; // Boolean | 合并请求和合并后
try {
    Hook result = apiInstance.postV5ReposOwnerRepoHooks(owner, repo, url, accessToken, password, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#postV5ReposOwnerRepoHooks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **url** | **String**| 远程HTTP URL |
 **accessToken** | **String**| 用户授权码 | [optional]
 **password** | **String**| 请求URL时会带上该密码，防止URL被恶意请求 | [optional]
 **pushEvents** | **Boolean**| Push代码到仓库 | [optional] [default to true]
 **tagPushEvents** | **Boolean**| 提交Tag到仓库 | [optional]
 **issuesEvents** | **Boolean**| 创建/关闭Issue | [optional]
 **noteEvents** | **Boolean**| 评论了Issue/代码等等 | [optional]
 **mergeRequestsEvents** | **Boolean**| 合并请求和合并后 | [optional]

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoHooksIdTests"></a>
# **postV5ReposOwnerRepoHooksIdTests**
> postV5ReposOwnerRepoHooksIdTests(owner, repo, id, accessToken)

测试WebHook是否发送成功

测试WebHook是否发送成功

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.WebhooksApi;


WebhooksApi apiInstance = new WebhooksApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | Webhook的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.postV5ReposOwnerRepoHooksIdTests(owner, repo, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling WebhooksApi#postV5ReposOwnerRepoHooksIdTests");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| Webhook的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

