
# Email

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**scope** | **List&lt;String&gt;** |  |  [optional]



