# LabelsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#deleteV5ReposOwnerRepoIssuesNumberLabels) | **DELETE** /v5/repos/{owner}/{repo}/issues/{number}/labels | 删除Issue所有标签
[**deleteV5ReposOwnerRepoIssuesNumberLabelsName**](LabelsApi.md#deleteV5ReposOwnerRepoIssuesNumberLabelsName) | **DELETE** /v5/repos/{owner}/{repo}/issues/{number}/labels/{name} | 删除Issue标签
[**deleteV5ReposOwnerRepoLabelsName**](LabelsApi.md#deleteV5ReposOwnerRepoLabelsName) | **DELETE** /v5/repos/{owner}/{repo}/labels/{name} | 删除一个仓库任务标签
[**getV5EnterprisesEnterpriseLabels**](LabelsApi.md#getV5EnterprisesEnterpriseLabels) | **GET** /v5/enterprises/{enterprise}/labels | 获取企业所有标签
[**getV5EnterprisesEnterpriseLabelsName**](LabelsApi.md#getV5EnterprisesEnterpriseLabelsName) | **GET** /v5/enterprises/{enterprise}/labels/{name} | 获取企业某个标签
[**getV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#getV5ReposOwnerRepoIssuesNumberLabels) | **GET** /v5/repos/{owner}/{repo}/issues/{number}/labels | 获取仓库任务的所有标签
[**getV5ReposOwnerRepoLabels**](LabelsApi.md#getV5ReposOwnerRepoLabels) | **GET** /v5/repos/{owner}/{repo}/labels | 获取仓库所有任务标签
[**getV5ReposOwnerRepoLabelsName**](LabelsApi.md#getV5ReposOwnerRepoLabelsName) | **GET** /v5/repos/{owner}/{repo}/labels/{name} | 根据标签名称获取单个标签
[**patchV5ReposOwnerRepoLabelsOriginalName**](LabelsApi.md#patchV5ReposOwnerRepoLabelsOriginalName) | **PATCH** /v5/repos/{owner}/{repo}/labels/{original_name} | 更新一个仓库任务标签
[**postV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#postV5ReposOwnerRepoIssuesNumberLabels) | **POST** /v5/repos/{owner}/{repo}/issues/{number}/labels | 创建Issue标签
[**postV5ReposOwnerRepoLabels**](LabelsApi.md#postV5ReposOwnerRepoLabels) | **POST** /v5/repos/{owner}/{repo}/labels | 创建仓库任务标签
[**putV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#putV5ReposOwnerRepoIssuesNumberLabels) | **PUT** /v5/repos/{owner}/{repo}/issues/{number}/labels | 替换Issue所有标签


<a name="deleteV5ReposOwnerRepoIssuesNumberLabels"></a>
# **deleteV5ReposOwnerRepoIssuesNumberLabels**
> deleteV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken)

删除Issue所有标签

删除Issue所有标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#deleteV5ReposOwnerRepoIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoIssuesNumberLabelsName"></a>
# **deleteV5ReposOwnerRepoIssuesNumberLabelsName**
> deleteV5ReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name, accessToken)

删除Issue标签

删除Issue标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String name = "name_example"; // String | 标签名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#deleteV5ReposOwnerRepoIssuesNumberLabelsName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **name** | **String**| 标签名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoLabelsName"></a>
# **deleteV5ReposOwnerRepoLabelsName**
> deleteV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken)

删除一个仓库任务标签

删除一个仓库任务标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String name = "name_example"; // String | 标签名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#deleteV5ReposOwnerRepoLabelsName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **name** | **String**| 标签名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseLabels"></a>
# **getV5EnterprisesEnterpriseLabels**
> List&lt;Label&gt; getV5EnterprisesEnterpriseLabels(enterprise, accessToken)

获取企业所有标签

获取企业所有标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    List<Label> result = apiInstance.getV5EnterprisesEnterpriseLabels(enterprise, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getV5EnterprisesEnterpriseLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseLabelsName"></a>
# **getV5EnterprisesEnterpriseLabelsName**
> Label getV5EnterprisesEnterpriseLabelsName(enterprise, name, accessToken)

获取企业某个标签

获取企业某个标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String name = "name_example"; // String | 标签名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Label result = apiInstance.getV5EnterprisesEnterpriseLabelsName(enterprise, name, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getV5EnterprisesEnterpriseLabelsName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **name** | **String**| 标签名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesNumberLabels"></a>
# **getV5ReposOwnerRepoIssuesNumberLabels**
> List&lt;Label&gt; getV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken)

获取仓库任务的所有标签

获取仓库任务的所有标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    List<Label> result = apiInstance.getV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getV5ReposOwnerRepoIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoLabels"></a>
# **getV5ReposOwnerRepoLabels**
> List&lt;Label&gt; getV5ReposOwnerRepoLabels(owner, repo, accessToken)

获取仓库所有任务标签

获取仓库所有任务标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    List<Label> result = apiInstance.getV5ReposOwnerRepoLabels(owner, repo, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getV5ReposOwnerRepoLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoLabelsName"></a>
# **getV5ReposOwnerRepoLabelsName**
> Label getV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken)

根据标签名称获取单个标签

根据标签名称获取单个标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String name = "name_example"; // String | 标签名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Label result = apiInstance.getV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#getV5ReposOwnerRepoLabelsName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **name** | **String**| 标签名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoLabelsOriginalName"></a>
# **patchV5ReposOwnerRepoLabelsOriginalName**
> Label patchV5ReposOwnerRepoLabelsOriginalName(owner, repo, originalName, accessToken, name, color)

更新一个仓库任务标签

更新一个仓库任务标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String originalName = "originalName_example"; // String | 标签原有名称
String accessToken = "accessToken_example"; // String | 用户授权码
String name = "name_example"; // String | 标签新名称
String color = "color_example"; // String | 标签新颜色
try {
    Label result = apiInstance.patchV5ReposOwnerRepoLabelsOriginalName(owner, repo, originalName, accessToken, name, color);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#patchV5ReposOwnerRepoLabelsOriginalName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **originalName** | **String**| 标签原有名称 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **name** | **String**| 标签新名称 | [optional]
 **color** | **String**| 标签新颜色 | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoIssuesNumberLabels"></a>
# **postV5ReposOwnerRepoIssuesNumberLabels**
> Label postV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken, body)

创建Issue标签

创建Issue标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
List<String> body = Arrays.asList("body_example"); // List<String> | 标签名数组，如: [\"feat\", \"bug\"]
try {
    Label result = apiInstance.postV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#postV5ReposOwnerRepoIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **body** | [**List&lt;String&gt;**](String.md)| 标签名数组，如: [\&quot;feat\&quot;, \&quot;bug\&quot;] | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoLabels"></a>
# **postV5ReposOwnerRepoLabels**
> Label postV5ReposOwnerRepoLabels(owner, repo, name, color, accessToken)

创建仓库任务标签

创建仓库任务标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String name = "name_example"; // String | 标签名称
String color = "color_example"; // String | 标签颜色。为6位的数字，如: 000000
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Label result = apiInstance.postV5ReposOwnerRepoLabels(owner, repo, name, color, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#postV5ReposOwnerRepoLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **name** | **String**| 标签名称 |
 **color** | **String**| 标签颜色。为6位的数字，如: 000000 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoIssuesNumberLabels"></a>
# **putV5ReposOwnerRepoIssuesNumberLabels**
> Label putV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken, body)

替换Issue所有标签

替换Issue所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.LabelsApi;


LabelsApi apiInstance = new LabelsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
List<String> body = Arrays.asList("body_example"); // List<String> | 标签名数组，如: [\"feat\", \"bug\"]
try {
    Label result = apiInstance.putV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LabelsApi#putV5ReposOwnerRepoIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **body** | [**List&lt;String&gt;**](String.md)| 标签名数组，如: [\&quot;feat\&quot;, \&quot;bug\&quot;] | [optional]

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

