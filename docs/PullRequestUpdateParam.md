
# PullRequestUpdateParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | 用户授权码 |  [optional]
**title** | **String** | 可选。Pull Request 标题 |  [optional]
**body** | **String** | 可选。Pull Request 内容 |  [optional]
**state** | [**StateEnum**](#StateEnum) | 可选。Pull Request 状态 |  [optional]
**milestoneNumber** | **Integer** | 可选。里程碑序号(id) |  [optional]
**labels** | **String** | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance |  [optional]


<a name="StateEnum"></a>
## Enum: StateEnum
Name | Value
---- | -----
OPEN | &quot;open&quot;
CLOSED | &quot;closed&quot;



