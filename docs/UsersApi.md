# UsersApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5UserFollowingUsername**](UsersApi.md#deleteV5UserFollowingUsername) | **DELETE** /v5/user/following/{username} | 取消关注一个用户
[**deleteV5UserKeysId**](UsersApi.md#deleteV5UserKeysId) | **DELETE** /v5/user/keys/{id} | 删除一个公钥
[**getV5User**](UsersApi.md#getV5User) | **GET** /v5/user | 获取授权用户的资料
[**getV5UserFollowers**](UsersApi.md#getV5UserFollowers) | **GET** /v5/user/followers | 列出授权用户的关注者
[**getV5UserFollowing**](UsersApi.md#getV5UserFollowing) | **GET** /v5/user/following | 列出授权用户正关注的用户
[**getV5UserFollowingUsername**](UsersApi.md#getV5UserFollowingUsername) | **GET** /v5/user/following/{username} | 检查授权用户是否关注了一个用户
[**getV5UserKeys**](UsersApi.md#getV5UserKeys) | **GET** /v5/user/keys | 列出授权用户的所有公钥
[**getV5UserKeysId**](UsersApi.md#getV5UserKeysId) | **GET** /v5/user/keys/{id} | 获取一个公钥
[**getV5UserNamespace**](UsersApi.md#getV5UserNamespace) | **GET** /v5/user/namespace | 获取授权用户的一个 Namespace
[**getV5UserNamespaces**](UsersApi.md#getV5UserNamespaces) | **GET** /v5/user/namespaces | 列出授权用户所有的 Namespace
[**getV5UsersUsername**](UsersApi.md#getV5UsersUsername) | **GET** /v5/users/{username} | 获取一个用户
[**getV5UsersUsernameFollowers**](UsersApi.md#getV5UsersUsernameFollowers) | **GET** /v5/users/{username}/followers | 列出指定用户的关注者
[**getV5UsersUsernameFollowing**](UsersApi.md#getV5UsersUsernameFollowing) | **GET** /v5/users/{username}/following | 列出指定用户正在关注的用户
[**getV5UsersUsernameFollowingTargetUser**](UsersApi.md#getV5UsersUsernameFollowingTargetUser) | **GET** /v5/users/{username}/following/{target_user} | 检查指定用户是否关注目标用户
[**getV5UsersUsernameKeys**](UsersApi.md#getV5UsersUsernameKeys) | **GET** /v5/users/{username}/keys | 列出指定用户的所有公钥
[**patchV5User**](UsersApi.md#patchV5User) | **PATCH** /v5/user | 更新授权用户的资料
[**postV5UserKeys**](UsersApi.md#postV5UserKeys) | **POST** /v5/user/keys | 添加一个公钥
[**putV5UserFollowingUsername**](UsersApi.md#putV5UserFollowingUsername) | **PUT** /v5/user/following/{username} | 关注一个用户


<a name="deleteV5UserFollowingUsername"></a>
# **deleteV5UserFollowingUsername**
> deleteV5UserFollowingUsername(username, accessToken)

取消关注一个用户

取消关注一个用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5UserFollowingUsername(username, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#deleteV5UserFollowingUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5UserKeysId"></a>
# **deleteV5UserKeysId**
> deleteV5UserKeysId(id, accessToken)

删除一个公钥

删除一个公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5UserKeysId(id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#deleteV5UserKeysId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5User"></a>
# **getV5User**
> User getV5User(accessToken)

获取授权用户的资料

获取授权用户的资料

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    User result = apiInstance.getV5User(accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5User");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserFollowers"></a>
# **getV5UserFollowers**
> List&lt;UserBasic&gt; getV5UserFollowers(accessToken, page, perPage)

列出授权用户的关注者

列出授权用户的关注者

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<UserBasic> result = apiInstance.getV5UserFollowers(accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UserFollowers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserFollowing"></a>
# **getV5UserFollowing**
> List&lt;UserBasic&gt; getV5UserFollowing(accessToken, page, perPage)

列出授权用户正关注的用户

列出授权用户正关注的用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<UserBasic> result = apiInstance.getV5UserFollowing(accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UserFollowing");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserFollowingUsername"></a>
# **getV5UserFollowingUsername**
> getV5UserFollowingUsername(username, accessToken)

检查授权用户是否关注了一个用户

检查授权用户是否关注了一个用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5UserFollowingUsername(username, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UserFollowingUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserKeys"></a>
# **getV5UserKeys**
> List&lt;SSHKey&gt; getV5UserKeys(accessToken, page, perPage)

列出授权用户的所有公钥

列出授权用户的所有公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<SSHKey> result = apiInstance.getV5UserKeys(accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UserKeys");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;SSHKey&gt;**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserKeysId"></a>
# **getV5UserKeysId**
> SSHKey getV5UserKeysId(id, accessToken)

获取一个公钥

获取一个公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    SSHKey result = apiInstance.getV5UserKeysId(id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UserKeysId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserNamespace"></a>
# **getV5UserNamespace**
> List&lt;Namespace&gt; getV5UserNamespace(path, accessToken)

获取授权用户的一个 Namespace

获取授权用户的一个 Namespace

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String path = "path_example"; // String | Namespace path
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    List<Namespace> result = apiInstance.getV5UserNamespace(path, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UserNamespace");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**| Namespace path |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**List&lt;Namespace&gt;**](Namespace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserNamespaces"></a>
# **getV5UserNamespaces**
> List&lt;Namespace&gt; getV5UserNamespaces(accessToken, mode)

列出授权用户所有的 Namespace

列出授权用户所有的 Namespace

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String mode = "mode_example"; // String | 参与方式: project(所有参与仓库的namepsce)、intrant(所加入的namespace)、all(包含前两者)，默认(intrant)
try {
    List<Namespace> result = apiInstance.getV5UserNamespaces(accessToken, mode);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UserNamespaces");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **mode** | **String**| 参与方式: project(所有参与仓库的namepsce)、intrant(所加入的namespace)、all(包含前两者)，默认(intrant) | [optional] [enum: project, intrant, all]

### Return type

[**List&lt;Namespace&gt;**](Namespace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsername"></a>
# **getV5UsersUsername**
> User getV5UsersUsername(username, accessToken)

获取一个用户

获取一个用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    User result = apiInstance.getV5UsersUsername(username, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UsersUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameFollowers"></a>
# **getV5UsersUsernameFollowers**
> List&lt;UserBasic&gt; getV5UsersUsernameFollowers(username, accessToken, page, perPage)

列出指定用户的关注者

列出指定用户的关注者

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<UserBasic> result = apiInstance.getV5UsersUsernameFollowers(username, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UsersUsernameFollowers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameFollowing"></a>
# **getV5UsersUsernameFollowing**
> List&lt;UserBasic&gt; getV5UsersUsernameFollowing(username, accessToken, page, perPage)

列出指定用户正在关注的用户

列出指定用户正在关注的用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<UserBasic> result = apiInstance.getV5UsersUsernameFollowing(username, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UsersUsernameFollowing");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameFollowingTargetUser"></a>
# **getV5UsersUsernameFollowingTargetUser**
> getV5UsersUsernameFollowingTargetUser(username, targetUser, accessToken)

检查指定用户是否关注目标用户

检查指定用户是否关注目标用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String username = "username_example"; // String | 用户名(username/login)
String targetUser = "targetUser_example"; // String | 目标用户的用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5UsersUsernameFollowingTargetUser(username, targetUser, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UsersUsernameFollowingTargetUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **targetUser** | **String**| 目标用户的用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameKeys"></a>
# **getV5UsersUsernameKeys**
> List&lt;SSHKeyBasic&gt; getV5UsersUsernameKeys(username, accessToken, page, perPage)

列出指定用户的所有公钥

列出指定用户的所有公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<SSHKeyBasic> result = apiInstance.getV5UsersUsernameKeys(username, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getV5UsersUsernameKeys");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;SSHKeyBasic&gt;**](SSHKeyBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5User"></a>
# **patchV5User**
> User patchV5User(accessToken, name, blog, weibo, bio)

更新授权用户的资料

更新授权用户的资料

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String name = "name_example"; // String | 昵称
String blog = "blog_example"; // String | 微博链接
String weibo = "weibo_example"; // String | 博客站点
String bio = "bio_example"; // String | 自我介绍
try {
    User result = apiInstance.patchV5User(accessToken, name, blog, weibo, bio);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#patchV5User");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **name** | **String**| 昵称 | [optional]
 **blog** | **String**| 微博链接 | [optional]
 **weibo** | **String**| 博客站点 | [optional]
 **bio** | **String**| 自我介绍 | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5UserKeys"></a>
# **postV5UserKeys**
> SSHKey postV5UserKeys(key, title, accessToken)

添加一个公钥

添加一个公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String key = "key_example"; // String | 公钥内容
String title = "title_example"; // String | 公钥名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    SSHKey result = apiInstance.postV5UserKeys(key, title, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#postV5UserKeys");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| 公钥内容 |
 **title** | **String**| 公钥名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5UserFollowingUsername"></a>
# **putV5UserFollowingUsername**
> putV5UserFollowingUsername(username, accessToken)

关注一个用户

关注一个用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.UsersApi;


UsersApi apiInstance = new UsersApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.putV5UserFollowingUsername(username, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#putV5UserFollowingUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

