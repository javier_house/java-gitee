
# RepositoryPostParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | 用户授权码 |  [optional]
**name** | **String** | 仓库名称 |  [optional]
**description** | **String** | 仓库描述 |  [optional]
**homepage** | **String** | 主页(eg: https://gitee.com) |  [optional]
**hasIssues** | **Boolean** | 允许提Issue与否。默认: 允许(true) |  [optional]
**hasWiki** | **Boolean** | 提供Wiki与否。默认: 提供(true) |  [optional]
**_public** | [**PublicEnum**](#PublicEnum) | 仓库开源类型。0(私有), 1(外部开源), 2(内部开源)，注：与private互斥，以public为主。 |  [optional]
**_private** | **Boolean** | 仓库公开或私有。默认: 公开(false)，注：与public互斥，以public为主。 |  [optional]
**autoInit** | **Boolean** | 值为true时则会用README初始化仓库。默认: 不初始化(false) |  [optional]
**gitignoreTemplate** | [**GitignoreTemplateEnum**](#GitignoreTemplateEnum) | Git Ingore模版 |  [optional]
**licenseTemplate** | [**LicenseTemplateEnum**](#LicenseTemplateEnum) | License模版 |  [optional]


<a name="PublicEnum"></a>
## Enum: PublicEnum
Name | Value
---- | -----
NUMBER_0 | 0
NUMBER_1 | 1
NUMBER_2 | 2


<a name="GitignoreTemplateEnum"></a>
## Enum: GitignoreTemplateEnum
Name | Value
---- | -----
ACTIONSCRIPT | &quot;Actionscript&quot;
ADA | &quot;Ada&quot;
AGDA | &quot;Agda&quot;
ANDROID | &quot;Android&quot;
APPENGINE | &quot;AppEngine&quot;
APPCELERATORTITANIUM | &quot;AppceleratorTitanium&quot;
ARCHLINUXPACKAGES | &quot;ArchLinuxPackages&quot;
AUTOTOOLS | &quot;Autotools&quot;
C | &quot;C&quot;
C_ | &quot;C++&quot;
CFWHEELS | &quot;CFWheels&quot;
CMAKE | &quot;CMake&quot;
CUDA | &quot;CUDA&quot;
CAKEPHP | &quot;CakePHP&quot;
CHEFCOOKBOOK | &quot;ChefCookbook&quot;
CLOJURE | &quot;Clojure&quot;
CODEIGNITER | &quot;CodeIgniter&quot;
COMMONLISP | &quot;CommonLisp&quot;
COMPOSER | &quot;Composer&quot;
CONCRETE5 | &quot;Concrete5&quot;
COQ | &quot;Coq&quot;
CRAFTCMS | &quot;CraftCMS&quot;
D | &quot;D&quot;
DM | &quot;DM&quot;
DART | &quot;Dart&quot;
DELPHI | &quot;Delphi&quot;
DRUPAL | &quot;Drupal&quot;
EPISERVER | &quot;EPiServer&quot;
EAGLE | &quot;Eagle&quot;
ELISP | &quot;Elisp&quot;
ELIXIR | &quot;Elixir&quot;
ELM | &quot;Elm&quot;
ERLANG | &quot;Erlang&quot;
EXPRESSIONENGINE | &quot;ExpressionEngine&quot;
EXTJS | &quot;ExtJs&quot;
FANCY | &quot;Fancy&quot;
FINALE | &quot;Finale&quot;
FLUTTER | &quot;Flutter&quot;
FORCEDOTCOM | &quot;ForceDotCom&quot;
FORTRAN | &quot;Fortran&quot;
FUELPHP | &quot;FuelPHP&quot;
GWT | &quot;GWT&quot;
GCOV | &quot;Gcov&quot;
GITBOOK | &quot;GitBook&quot;
GLOBAL_ANJUTA | &quot;Global/Anjuta&quot;
GLOBAL_ANSIBLE | &quot;Global/Ansible&quot;
GLOBAL_ARCHIVES | &quot;Global/Archives&quot;
GLOBAL_BACKUP | &quot;Global/Backup&quot;
GLOBAL_BAZAAR | &quot;Global/Bazaar&quot;
GLOBAL_BRICXCC | &quot;Global/BricxCC&quot;
GLOBAL_CVS | &quot;Global/CVS&quot;
GLOBAL_CALABASH | &quot;Global/Calabash&quot;
GLOBAL_CLOUD9 | &quot;Global/Cloud9&quot;
GLOBAL_CODEKIT | &quot;Global/CodeKit&quot;
GLOBAL_DARTEDITOR | &quot;Global/DartEditor&quot;
GLOBAL_DIFF | &quot;Global/Diff&quot;
GLOBAL_DREAMWEAVER | &quot;Global/Dreamweaver&quot;
GLOBAL_DROPBOX | &quot;Global/Dropbox&quot;
GLOBAL_ECLIPSE | &quot;Global/Eclipse&quot;
GLOBAL_EIFFELSTUDIO | &quot;Global/EiffelStudio&quot;
GLOBAL_EMACS | &quot;Global/Emacs&quot;
GLOBAL_ENSIME | &quot;Global/Ensime&quot;
GLOBAL_ESPRESSO | &quot;Global/Espresso&quot;
GLOBAL_FLEXBUILDER | &quot;Global/FlexBuilder&quot;
GLOBAL_GPG | &quot;Global/GPG&quot;
GLOBAL_IMAGES | &quot;Global/Images&quot;
GLOBAL_JDEVELOPER | &quot;Global/JDeveloper&quot;
GLOBAL_JENV | &quot;Global/JEnv&quot;
GLOBAL_JETBRAINS | &quot;Global/JetBrains&quot;
GLOBAL_KDEVELOP4 | &quot;Global/KDevelop4&quot;
GLOBAL_KATE | &quot;Global/Kate&quot;
GLOBAL_LAZARUS | &quot;Global/Lazarus&quot;
GLOBAL_LIBREOFFICE | &quot;Global/LibreOffice&quot;
GLOBAL_LINUX | &quot;Global/Linux&quot;
GLOBAL_LYX | &quot;Global/LyX&quot;
GLOBAL_MATLAB | &quot;Global/MATLAB&quot;
GLOBAL_MERCURIAL | &quot;Global/Mercurial&quot;
GLOBAL_MICROSOFTOFFICE | &quot;Global/MicrosoftOffice&quot;
GLOBAL_MODELSIM | &quot;Global/ModelSim&quot;
GLOBAL_MOMENTICS | &quot;Global/Momentics&quot;
GLOBAL_MONODEVELOP | &quot;Global/MonoDevelop&quot;
GLOBAL_NETBEANS | &quot;Global/NetBeans&quot;
GLOBAL_NINJA | &quot;Global/Ninja&quot;
GLOBAL_NOTEPADPP | &quot;Global/NotepadPP&quot;
GLOBAL_OCTAVE | &quot;Global/Octave&quot;
GLOBAL_OTTO | &quot;Global/Otto&quot;
GLOBAL_PSOCCREATOR | &quot;Global/PSoCCreator&quot;
GLOBAL_PATCH | &quot;Global/Patch&quot;
GLOBAL_PUTTY | &quot;Global/PuTTY&quot;
GLOBAL_REDCAR | &quot;Global/Redcar&quot;
GLOBAL_REDIS | &quot;Global/Redis&quot;
GLOBAL_SBT | &quot;Global/SBT&quot;
GLOBAL_SVN | &quot;Global/SVN&quot;
GLOBAL_SLICKEDIT | &quot;Global/SlickEdit&quot;
GLOBAL_STATA | &quot;Global/Stata&quot;
GLOBAL_SUBLIMETEXT | &quot;Global/SublimeText&quot;
GLOBAL_SYNOPSYSVCS | &quot;Global/SynopsysVCS&quot;
GLOBAL_TAGS | &quot;Global/Tags&quot;
GLOBAL_TEXTMATE | &quot;Global/TextMate&quot;
GLOBAL_TORTOISEGIT | &quot;Global/TortoiseGit&quot;
GLOBAL_VAGRANT | &quot;Global/Vagrant&quot;
GLOBAL_VIM | &quot;Global/Vim&quot;
GLOBAL_VIRTUALENV | &quot;Global/VirtualEnv&quot;
GLOBAL_VIRTUOSO | &quot;Global/Virtuoso&quot;
GLOBAL_VISUALSTUDIOCODE | &quot;Global/VisualStudioCode&quot;
GLOBAL_WEBMETHODS | &quot;Global/WebMethods&quot;
GLOBAL_WINDOWS | &quot;Global/Windows&quot;
GLOBAL_XCODE | &quot;Global/Xcode&quot;
GLOBAL_XILINXISE | &quot;Global/XilinxISE&quot;
GLOBAL_MACOS | &quot;Global/macOS&quot;
GO | &quot;Go&quot;
GODOT | &quot;Godot&quot;
GRADLE | &quot;Gradle&quot;
GRAILS | &quot;Grails&quot;
HASKELL | &quot;Haskell&quot;
IGORPRO | &quot;IGORPro&quot;
IDRIS | &quot;Idris&quot;
JBOSS | &quot;JBoss&quot;
JAVA | &quot;Java&quot;
JEKYLL | &quot;Jekyll&quot;
JOOMLA | &quot;Joomla&quot;
JULIA | &quot;Julia&quot;
KICAD | &quot;KiCad&quot;
KOHANA | &quot;Kohana&quot;
KOTLIN | &quot;Kotlin&quot;
LABVIEW | &quot;LabVIEW&quot;
LARAVEL | &quot;Laravel&quot;
LEININGEN | &quot;Leiningen&quot;
LEMONSTAND | &quot;LemonStand&quot;
LILYPOND | &quot;Lilypond&quot;
LITHIUM | &quot;Lithium&quot;
LUA | &quot;Lua&quot;
MAGENTO | &quot;Magento&quot;
MAVEN | &quot;Maven&quot;
MERCURY | &quot;Mercury&quot;
METAPROGRAMMINGSYSTEM | &quot;MetaProgrammingSystem&quot;
MINIPROGRAM | &quot;MiniProgram&quot;
NANOC | &quot;Nanoc&quot;
NIM | &quot;Nim&quot;
NODE | &quot;Node&quot;
OCAML | &quot;OCaml&quot;
OBJECTIVE_C | &quot;Objective-C&quot;
OPA | &quot;Opa&quot;
OPENCART | &quot;OpenCart&quot;
ORACLEFORMS | &quot;OracleForms&quot;
PACKER | &quot;Packer&quot;
PERL | &quot;Perl&quot;
PERL6 | &quot;Perl6&quot;
PHALCON | &quot;Phalcon&quot;
PLAYFRAMEWORK | &quot;PlayFramework&quot;
PLONE | &quot;Plone&quot;
PRESTASHOP | &quot;Prestashop&quot;
PROCESSING | &quot;Processing&quot;
PURESCRIPT | &quot;PureScript&quot;
PYTHON | &quot;Python&quot;
QOOXDOO | &quot;Qooxdoo&quot;
QT | &quot;Qt&quot;
R | &quot;R&quot;
ROS | &quot;ROS&quot;
RAILS | &quot;Rails&quot;
RHODESRHOMOBILE | &quot;RhodesRhomobile&quot;
RUBY | &quot;Ruby&quot;
RUST | &quot;Rust&quot;
SCONS | &quot;SCons&quot;
SASS | &quot;Sass&quot;
SCALA | &quot;Scala&quot;
SCHEME | &quot;Scheme&quot;
SCRIVENER | &quot;Scrivener&quot;
SDCC | &quot;Sdcc&quot;
SEAMGEN | &quot;SeamGen&quot;
SKETCHUP | &quot;SketchUp&quot;
SMALLTALK | &quot;Smalltalk&quot;
STELLA | &quot;Stella&quot;
SUGARCRM | &quot;SugarCRM&quot;
SWIFT | &quot;Swift&quot;
SYMFONY | &quot;Symfony&quot;
SYMPHONYCMS | &quot;SymphonyCMS&quot;
TEX | &quot;TeX&quot;
TERRAFORM | &quot;Terraform&quot;
TEXTPATTERN | &quot;Textpattern&quot;
TURBOGEARS2 | &quot;TurboGears2&quot;
TYPO3 | &quot;Typo3&quot;
UMBRACO | &quot;Umbraco&quot;
UNITY | &quot;Unity&quot;
UNREALENGINE | &quot;UnrealEngine&quot;
VVVV | &quot;VVVV&quot;
VISUALSTUDIO | &quot;VisualStudio&quot;
WAF | &quot;Waf&quot;
WORDPRESS | &quot;WordPress&quot;
XOJO | &quot;Xojo&quot;
YEOMAN | &quot;Yeoman&quot;
YII | &quot;Yii&quot;
ZENDFRAMEWORK | &quot;ZendFramework&quot;
ZEPHIR | &quot;Zephir&quot;


<a name="LicenseTemplateEnum"></a>
## Enum: LicenseTemplateEnum
Name | Value
---- | -----
MULANPSL_1_0 | &quot;MulanPSL-1.0&quot;
AFL_3_0 | &quot;AFL-3.0&quot;
AGPL_3_0 | &quot;AGPL-3.0&quot;
APACHE_2_0 | &quot;Apache-2.0&quot;
ARTISTIC_2_0 | &quot;Artistic-2.0&quot;
BSD_2_CLAUSE | &quot;BSD-2-Clause&quot;
BSD_3_CLAUSE | &quot;BSD-3-Clause&quot;
BSD_3_CLAUSE_CLEAR | &quot;BSD-3-Clause-Clear&quot;
BSL_1_0 | &quot;BSL-1.0&quot;
CC_BY_4_0 | &quot;CC-BY-4.0&quot;
CC_BY_SA_4_0 | &quot;CC-BY-SA-4.0&quot;
CC0_1_0 | &quot;CC0-1.0&quot;
ECL_2_0 | &quot;ECL-2.0&quot;
EPL_1_0 | &quot;EPL-1.0&quot;
EUPL_1_1 | &quot;EUPL-1.1&quot;
GPL_2_0 | &quot;GPL-2.0&quot;
GPL_3_0 | &quot;GPL-3.0&quot;
ISC | &quot;ISC&quot;
LGPL_2_1 | &quot;LGPL-2.1&quot;
LGPL_3_0 | &quot;LGPL-3.0&quot;
LPPL_1_3C | &quot;LPPL-1.3c&quot;
MIT | &quot;MIT&quot;
MPL_2_0 | &quot;MPL-2.0&quot;
MS_PL | &quot;MS-PL&quot;
MS_RL | &quot;MS-RL&quot;
NCSA | &quot;NCSA&quot;
OFL_1_1 | &quot;OFL-1.1&quot;
OSL_3_0 | &quot;OSL-3.0&quot;
POSTGRESQL | &quot;PostgreSQL&quot;
UNLICENSE | &quot;Unlicense&quot;
WTFPL | &quot;WTFPL&quot;
ZLIB | &quot;Zlib&quot;



