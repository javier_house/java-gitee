# EmailsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getV5Emails**](EmailsApi.md#getV5Emails) | **GET** /v5/emails | 获取授权用户的所有邮箱


<a name="getV5Emails"></a>
# **getV5Emails**
> List&lt;Email&gt; getV5Emails(accessToken)

获取授权用户的所有邮箱

获取授权用户的所有邮箱

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EmailsApi;


EmailsApi apiInstance = new EmailsApi();
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    List<Email> result = apiInstance.getV5Emails(accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EmailsApi#getV5Emails");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**List&lt;Email&gt;**](Email.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

