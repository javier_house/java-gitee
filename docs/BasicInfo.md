
# BasicInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  |  [optional]
**ref** | **String** |  |  [optional]
**sha** | **String** |  |  [optional]
**user** | [**UserBasic**](UserBasic.md) |  |  [optional]
**repo** | [**Project**](Project.md) |  |  [optional]



