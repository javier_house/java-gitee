
# RepoPatchParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | 用户授权码 |  [optional]
**name** | **String** | 仓库名称 |  [optional]
**description** | **String** | 仓库描述 |  [optional]
**homepage** | **String** | 主页(eg: https://gitee.com) |  [optional]
**hasIssues** | **String** | 允许提Issue与否。默认: 允许(true) |  [optional]
**hasWiki** | **String** | 提供Wiki与否。默认: 提供(true) |  [optional]
**_private** | **String** | 仓库公开或私有。 |  [optional]
**defaultBranch** | **String** | 更新默认分支 |  [optional]



