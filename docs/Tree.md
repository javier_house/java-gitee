
# Tree

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**tree** | [**List&lt;TreeBasic&gt;**](TreeBasic.md) |  |  [optional]
**truncated** | **String** |  |  [optional]



