
# IssueCommentPostParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | 用户授权码 |  [optional]
**body** | **String** | The contents of the comment |  [optional]



