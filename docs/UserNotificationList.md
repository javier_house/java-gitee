
# UserNotificationList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalCount** | **Integer** |  |  [optional]
**list** | [**List&lt;UserNotification&gt;**](UserNotification.md) | 通知列表 |  [optional]



