
# UserMessageList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalCount** | **Integer** |  |  [optional]
**list** | [**List&lt;UserMessage&gt;**](UserMessage.md) | 私信列表 |  [optional]



