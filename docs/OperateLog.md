
# OperateLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**icon** | **String** |  |  [optional]
**user** | **String** |  |  [optional]
**content** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]



