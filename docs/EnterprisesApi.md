# EnterprisesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5EnterprisesEnterpriseMembersUsername**](EnterprisesApi.md#deleteV5EnterprisesEnterpriseMembersUsername) | **DELETE** /v5/enterprises/{enterprise}/members/{username} | 移除企业成员
[**deleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId**](EnterprisesApi.md#deleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId) | **DELETE** /v5/enterprises/{enterprise}/week_reports/{report_id}/comments/{id} | 删除周报某个评论
[**getV5EnterprisesEnterprise**](EnterprisesApi.md#getV5EnterprisesEnterprise) | **GET** /v5/enterprises/{enterprise} | 获取一个企业
[**getV5EnterprisesEnterpriseMembers**](EnterprisesApi.md#getV5EnterprisesEnterpriseMembers) | **GET** /v5/enterprises/{enterprise}/members | 列出企业的所有成员
[**getV5EnterprisesEnterpriseMembersUsername**](EnterprisesApi.md#getV5EnterprisesEnterpriseMembersUsername) | **GET** /v5/enterprises/{enterprise}/members/{username} | 获取企业的一个成员
[**getV5EnterprisesEnterpriseUsersUsernameWeekReports**](EnterprisesApi.md#getV5EnterprisesEnterpriseUsersUsernameWeekReports) | **GET** /v5/enterprises/{enterprise}/users/{username}/week_reports | 个人周报列表
[**getV5EnterprisesEnterpriseWeekReports**](EnterprisesApi.md#getV5EnterprisesEnterpriseWeekReports) | **GET** /v5/enterprises/{enterprise}/week_reports | 企业成员周报列表
[**getV5EnterprisesEnterpriseWeekReportsId**](EnterprisesApi.md#getV5EnterprisesEnterpriseWeekReportsId) | **GET** /v5/enterprises/{enterprise}/week_reports/{id} | 周报详情
[**getV5EnterprisesEnterpriseWeekReportsIdComments**](EnterprisesApi.md#getV5EnterprisesEnterpriseWeekReportsIdComments) | **GET** /v5/enterprises/{enterprise}/week_reports/{id}/comments | 某个周报评论列表
[**getV5UserEnterprises**](EnterprisesApi.md#getV5UserEnterprises) | **GET** /v5/user/enterprises | 列出授权用户所属的企业
[**patchV5EnterprisesEnterpriseWeekReportId**](EnterprisesApi.md#patchV5EnterprisesEnterpriseWeekReportId) | **PATCH** /v5/enterprises/{enterprise}/week_report/{id} | 编辑周报
[**postV5EnterprisesEnterpriseMembers**](EnterprisesApi.md#postV5EnterprisesEnterpriseMembers) | **POST** /v5/enterprises/{enterprise}/members | 添加或邀请企业成员
[**postV5EnterprisesEnterpriseWeekReport**](EnterprisesApi.md#postV5EnterprisesEnterpriseWeekReport) | **POST** /v5/enterprises/{enterprise}/week_report | 新建周报
[**postV5EnterprisesEnterpriseWeekReportsIdComment**](EnterprisesApi.md#postV5EnterprisesEnterpriseWeekReportsIdComment) | **POST** /v5/enterprises/{enterprise}/week_reports/{id}/comment | 评论周报
[**putV5EnterprisesEnterpriseMembersUsername**](EnterprisesApi.md#putV5EnterprisesEnterpriseMembersUsername) | **PUT** /v5/enterprises/{enterprise}/members/{username} | 修改企业成员权限或备注


<a name="deleteV5EnterprisesEnterpriseMembersUsername"></a>
# **deleteV5EnterprisesEnterpriseMembersUsername**
> deleteV5EnterprisesEnterpriseMembersUsername(enterprise, username, accessToken)

移除企业成员

移除企业成员

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5EnterprisesEnterpriseMembersUsername(enterprise, username, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#deleteV5EnterprisesEnterpriseMembersUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId"></a>
# **deleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId**
> deleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId(enterprise, reportId, id, accessToken)

删除周报某个评论

删除周报某个评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer reportId = 56; // Integer | 周报ID
Integer id = 56; // Integer | 评论ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId(enterprise, reportId, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#deleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **reportId** | **Integer**| 周报ID |
 **id** | **Integer**| 评论ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterprise"></a>
# **getV5EnterprisesEnterprise**
> EnterpriseBasic getV5EnterprisesEnterprise(enterprise, accessToken)

获取一个企业

获取一个企业

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    EnterpriseBasic result = apiInstance.getV5EnterprisesEnterprise(enterprise, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getV5EnterprisesEnterprise");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**EnterpriseBasic**](EnterpriseBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseMembers"></a>
# **getV5EnterprisesEnterpriseMembers**
> List&lt;EnterpriseMember&gt; getV5EnterprisesEnterpriseMembers(enterprise, accessToken, role)

列出企业的所有成员

列出企业的所有成员

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String role = "all"; // String | 根据角色筛选成员
try {
    List<EnterpriseMember> result = apiInstance.getV5EnterprisesEnterpriseMembers(enterprise, accessToken, role);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getV5EnterprisesEnterpriseMembers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **role** | **String**| 根据角色筛选成员 | [optional] [default to all] [enum: all, admin, member]

### Return type

[**List&lt;EnterpriseMember&gt;**](EnterpriseMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseMembersUsername"></a>
# **getV5EnterprisesEnterpriseMembersUsername**
> EnterpriseMember getV5EnterprisesEnterpriseMembersUsername(enterprise, username, accessToken)

获取企业的一个成员

获取企业的一个成员

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    EnterpriseMember result = apiInstance.getV5EnterprisesEnterpriseMembersUsername(enterprise, username, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getV5EnterprisesEnterpriseMembersUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**EnterpriseMember**](EnterpriseMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseUsersUsernameWeekReports"></a>
# **getV5EnterprisesEnterpriseUsersUsernameWeekReports**
> List&lt;WeekReport&gt; getV5EnterprisesEnterpriseUsersUsernameWeekReports(enterprise, username, accessToken, page, perPage)

个人周报列表

个人周报列表

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<WeekReport> result = apiInstance.getV5EnterprisesEnterpriseUsersUsernameWeekReports(enterprise, username, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getV5EnterprisesEnterpriseUsersUsernameWeekReports");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;WeekReport&gt;**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseWeekReports"></a>
# **getV5EnterprisesEnterpriseWeekReports**
> List&lt;WeekReport&gt; getV5EnterprisesEnterpriseWeekReports(enterprise, accessToken, page, perPage, username, year, weekIndex, date)

企业成员周报列表

企业成员周报列表

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String username = "username_example"; // String | 用户名(username/login)
Integer year = 56; // Integer | 周报所属年
Integer weekIndex = 56; // Integer | 周报所属周
String date = "date_example"; // String | 周报日期(格式：2019-03-25)
try {
    List<WeekReport> result = apiInstance.getV5EnterprisesEnterpriseWeekReports(enterprise, accessToken, page, perPage, username, year, weekIndex, date);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getV5EnterprisesEnterpriseWeekReports");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **username** | **String**| 用户名(username/login) | [optional]
 **year** | **Integer**| 周报所属年 | [optional]
 **weekIndex** | **Integer**| 周报所属周 | [optional]
 **date** | **String**| 周报日期(格式：2019-03-25) | [optional]

### Return type

[**List&lt;WeekReport&gt;**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseWeekReportsId"></a>
# **getV5EnterprisesEnterpriseWeekReportsId**
> WeekReport getV5EnterprisesEnterpriseWeekReportsId(enterprise, id, accessToken)

周报详情

周报详情

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer id = 56; // Integer | 周报ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    WeekReport result = apiInstance.getV5EnterprisesEnterpriseWeekReportsId(enterprise, id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getV5EnterprisesEnterpriseWeekReportsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **id** | **Integer**| 周报ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**WeekReport**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseWeekReportsIdComments"></a>
# **getV5EnterprisesEnterpriseWeekReportsIdComments**
> List&lt;Note&gt; getV5EnterprisesEnterpriseWeekReportsIdComments(enterprise, id, accessToken, page, perPage)

某个周报评论列表

某个周报评论列表

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer id = 56; // Integer | 周报ID
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Note> result = apiInstance.getV5EnterprisesEnterpriseWeekReportsIdComments(enterprise, id, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getV5EnterprisesEnterpriseWeekReportsIdComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **id** | **Integer**| 周报ID |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Note&gt;**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserEnterprises"></a>
# **getV5UserEnterprises**
> List&lt;EnterpriseBasic&gt; getV5UserEnterprises(accessToken, page, perPage, admin)

列出授权用户所属的企业

列出授权用户所属的企业

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
Boolean admin = true; // Boolean | 只列出授权用户管理的企业
try {
    List<EnterpriseBasic> result = apiInstance.getV5UserEnterprises(accessToken, page, perPage, admin);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#getV5UserEnterprises");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **admin** | **Boolean**| 只列出授权用户管理的企业 | [optional] [default to true]

### Return type

[**List&lt;EnterpriseBasic&gt;**](EnterpriseBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5EnterprisesEnterpriseWeekReportId"></a>
# **patchV5EnterprisesEnterpriseWeekReportId**
> WeekReport patchV5EnterprisesEnterpriseWeekReportId(enterprise, id, content, accessToken)

编辑周报

编辑周报

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer id = 56; // Integer | 周报ID
String content = "content_example"; // String | 周报内容
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    WeekReport result = apiInstance.patchV5EnterprisesEnterpriseWeekReportId(enterprise, id, content, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#patchV5EnterprisesEnterpriseWeekReportId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **id** | **Integer**| 周报ID |
 **content** | **String**| 周报内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**WeekReport**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5EnterprisesEnterpriseMembers"></a>
# **postV5EnterprisesEnterpriseMembers**
> postV5EnterprisesEnterpriseMembers(enterprise, accessToken, username, email, outsourced, role, name)

添加或邀请企业成员

添加或邀请企业成员

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String username = "username_example"; // String | 需要邀请的码云用户名(username/login)，username,email至少填写一个
String email = "email_example"; // String | 要添加邮箱地址，若该邮箱未注册则自动创建帐号。username,email至少填写一个
Boolean outsourced = true; // Boolean | 是否企业外包成员，默认：否
String role = "member"; // String | 企业角色，默认普通成员
String name = "name_example"; // String | 企业成员真实姓名（备注）
try {
    apiInstance.postV5EnterprisesEnterpriseMembers(enterprise, accessToken, username, email, outsourced, role, name);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#postV5EnterprisesEnterpriseMembers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **username** | **String**| 需要邀请的码云用户名(username/login)，username,email至少填写一个 | [optional]
 **email** | **String**| 要添加邮箱地址，若该邮箱未注册则自动创建帐号。username,email至少填写一个 | [optional]
 **outsourced** | **Boolean**| 是否企业外包成员，默认：否 | [optional]
 **role** | **String**| 企业角色，默认普通成员 | [optional] [default to member] [enum: admin, member]
 **name** | **String**| 企业成员真实姓名（备注） | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5EnterprisesEnterpriseWeekReport"></a>
# **postV5EnterprisesEnterpriseWeekReport**
> WeekReport postV5EnterprisesEnterpriseWeekReport(enterprise, year, content, weekIndex, username, accessToken, date)

新建周报

新建周报

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer year = 56; // Integer | 周报所属年
String content = "content_example"; // String | 周报内容
Integer weekIndex = 56; // Integer | 周报所属周
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String date = "date_example"; // String | 周报日期(格式：2019-03-25)
try {
    WeekReport result = apiInstance.postV5EnterprisesEnterpriseWeekReport(enterprise, year, content, weekIndex, username, accessToken, date);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#postV5EnterprisesEnterpriseWeekReport");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **year** | **Integer**| 周报所属年 |
 **content** | **String**| 周报内容 |
 **weekIndex** | **Integer**| 周报所属周 |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **date** | **String**| 周报日期(格式：2019-03-25) | [optional]

### Return type

[**WeekReport**](WeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5EnterprisesEnterpriseWeekReportsIdComment"></a>
# **postV5EnterprisesEnterpriseWeekReportsIdComment**
> Note postV5EnterprisesEnterpriseWeekReportsIdComment(enterprise, id, body, accessToken)

评论周报

评论周报

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
Integer id = 56; // Integer | 周报ID
String body = "body_example"; // String | 评论的内容
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Note result = apiInstance.postV5EnterprisesEnterpriseWeekReportsIdComment(enterprise, id, body, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#postV5EnterprisesEnterpriseWeekReportsIdComment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **id** | **Integer**| 周报ID |
 **body** | **String**| 评论的内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5EnterprisesEnterpriseMembersUsername"></a>
# **putV5EnterprisesEnterpriseMembersUsername**
> EnterpriseMember putV5EnterprisesEnterpriseMembersUsername(enterprise, username, accessToken, outsourced, role, active, name)

修改企业成员权限或备注

修改企业成员权限或备注

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.EnterprisesApi;


EnterprisesApi apiInstance = new EnterprisesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean outsourced = true; // Boolean | 是否企业外包成员，默认：否
String role = "member"; // String | 企业角色，默认普通成员
Boolean active = true; // Boolean | 是否可访问企业资源，默认:是。（若选否则禁止该用户访问企业资源）
String name = "name_example"; // String | 企业成员真实姓名（备注）
try {
    EnterpriseMember result = apiInstance.putV5EnterprisesEnterpriseMembersUsername(enterprise, username, accessToken, outsourced, role, active, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EnterprisesApi#putV5EnterprisesEnterpriseMembersUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **outsourced** | **Boolean**| 是否企业外包成员，默认：否 | [optional]
 **role** | **String**| 企业角色，默认普通成员 | [optional] [default to member] [enum: admin, member]
 **active** | **Boolean**| 是否可访问企业资源，默认:是。（若选否则禁止该用户访问企业资源） | [optional] [default to true]
 **name** | **String**| 企业成员真实姓名（备注） | [optional]

### Return type

[**EnterpriseMember**](EnterpriseMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

