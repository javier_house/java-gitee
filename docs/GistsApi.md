# GistsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5GistsGistIdCommentsId**](GistsApi.md#deleteV5GistsGistIdCommentsId) | **DELETE** /v5/gists/{gist_id}/comments/{id} | 删除代码片段的评论
[**deleteV5GistsId**](GistsApi.md#deleteV5GistsId) | **DELETE** /v5/gists/{id} | 删除指定代码片段
[**deleteV5GistsIdStar**](GistsApi.md#deleteV5GistsIdStar) | **DELETE** /v5/gists/{id}/star | 取消Star代码片段
[**getV5Gists**](GistsApi.md#getV5Gists) | **GET** /v5/gists | 获取代码片段
[**getV5GistsGistIdComments**](GistsApi.md#getV5GistsGistIdComments) | **GET** /v5/gists/{gist_id}/comments | 获取代码片段的评论
[**getV5GistsGistIdCommentsId**](GistsApi.md#getV5GistsGistIdCommentsId) | **GET** /v5/gists/{gist_id}/comments/{id} | 获取单条代码片段的评论
[**getV5GistsId**](GistsApi.md#getV5GistsId) | **GET** /v5/gists/{id} | 获取单条代码片段
[**getV5GistsIdCommits**](GistsApi.md#getV5GistsIdCommits) | **GET** /v5/gists/{id}/commits | 获取代码片段的commit
[**getV5GistsIdForks**](GistsApi.md#getV5GistsIdForks) | **GET** /v5/gists/{id}/forks | 获取 Fork 了指定代码片段的列表
[**getV5GistsIdStar**](GistsApi.md#getV5GistsIdStar) | **GET** /v5/gists/{id}/star | 判断代码片段是否已Star
[**getV5GistsPublic**](GistsApi.md#getV5GistsPublic) | **GET** /v5/gists/public | 获取公开的代码片段
[**getV5GistsStarred**](GistsApi.md#getV5GistsStarred) | **GET** /v5/gists/starred | 获取用户Star的代码片段
[**getV5UsersUsernameGists**](GistsApi.md#getV5UsersUsernameGists) | **GET** /v5/users/{username}/gists | 获取指定用户的公开代码片段
[**patchV5GistsGistIdCommentsId**](GistsApi.md#patchV5GistsGistIdCommentsId) | **PATCH** /v5/gists/{gist_id}/comments/{id} | 修改代码片段的评论
[**patchV5GistsId**](GistsApi.md#patchV5GistsId) | **PATCH** /v5/gists/{id} | 修改代码片段
[**postV5Gists**](GistsApi.md#postV5Gists) | **POST** /v5/gists | 创建代码片段
[**postV5GistsGistIdComments**](GistsApi.md#postV5GistsGistIdComments) | **POST** /v5/gists/{gist_id}/comments | 增加代码片段的评论
[**postV5GistsIdForks**](GistsApi.md#postV5GistsIdForks) | **POST** /v5/gists/{id}/forks | Fork代码片段
[**putV5GistsIdStar**](GistsApi.md#putV5GistsIdStar) | **PUT** /v5/gists/{id}/star | Star代码片段


<a name="deleteV5GistsGistIdCommentsId"></a>
# **deleteV5GistsGistIdCommentsId**
> deleteV5GistsGistIdCommentsId(gistId, id, accessToken)

删除代码片段的评论

删除代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5GistsGistIdCommentsId(gistId, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#deleteV5GistsGistIdCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5GistsId"></a>
# **deleteV5GistsId**
> deleteV5GistsId(id, accessToken)

删除指定代码片段

删除指定代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5GistsId(id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#deleteV5GistsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5GistsIdStar"></a>
# **deleteV5GistsIdStar**
> deleteV5GistsIdStar(id, accessToken)

取消Star代码片段

取消Star代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5GistsIdStar(id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#deleteV5GistsIdStar");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5Gists"></a>
# **getV5Gists**
> List&lt;Code&gt; getV5Gists(accessToken, since, page, perPage)

获取代码片段

获取代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Code> result = apiInstance.getV5Gists(accessToken, since, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5Gists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Code&gt;**](Code.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GistsGistIdComments"></a>
# **getV5GistsGistIdComments**
> List&lt;CodeComment&gt; getV5GistsGistIdComments(gistId, accessToken, page, perPage)

获取代码片段的评论

获取代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<CodeComment> result = apiInstance.getV5GistsGistIdComments(gistId, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5GistsGistIdComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;CodeComment&gt;**](CodeComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GistsGistIdCommentsId"></a>
# **getV5GistsGistIdCommentsId**
> CodeComment getV5GistsGistIdCommentsId(gistId, id, accessToken)

获取单条代码片段的评论

获取单条代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    CodeComment result = apiInstance.getV5GistsGistIdCommentsId(gistId, id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5GistsGistIdCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CodeComment**](CodeComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GistsId"></a>
# **getV5GistsId**
> CodeForksHistory getV5GistsId(id, accessToken)

获取单条代码片段

获取单条代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    CodeForksHistory result = apiInstance.getV5GistsId(id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5GistsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CodeForksHistory**](CodeForksHistory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GistsIdCommits"></a>
# **getV5GistsIdCommits**
> CodeForksHistory getV5GistsIdCommits(id, accessToken)

获取代码片段的commit

获取代码片段的commit

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    CodeForksHistory result = apiInstance.getV5GistsIdCommits(id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5GistsIdCommits");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CodeForksHistory**](CodeForksHistory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GistsIdForks"></a>
# **getV5GistsIdForks**
> CodeForks getV5GistsIdForks(id, accessToken, page, perPage)

获取 Fork 了指定代码片段的列表

获取 Fork 了指定代码片段的列表

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    CodeForks result = apiInstance.getV5GistsIdForks(id, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5GistsIdForks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**CodeForks**](CodeForks.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GistsIdStar"></a>
# **getV5GistsIdStar**
> getV5GistsIdStar(id, accessToken)

判断代码片段是否已Star

判断代码片段是否已Star

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5GistsIdStar(id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5GistsIdStar");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GistsPublic"></a>
# **getV5GistsPublic**
> List&lt;Code&gt; getV5GistsPublic(accessToken, since, page, perPage)

获取公开的代码片段

获取公开的代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Code> result = apiInstance.getV5GistsPublic(accessToken, since, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5GistsPublic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Code&gt;**](Code.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GistsStarred"></a>
# **getV5GistsStarred**
> List&lt;Code&gt; getV5GistsStarred(accessToken, since, page, perPage)

获取用户Star的代码片段

获取用户Star的代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Code> result = apiInstance.getV5GistsStarred(accessToken, since, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5GistsStarred");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Code&gt;**](Code.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameGists"></a>
# **getV5UsersUsernameGists**
> List&lt;Code&gt; getV5UsersUsernameGists(username, accessToken, page, perPage)

获取指定用户的公开代码片段

获取指定用户的公开代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Code> result = apiInstance.getV5UsersUsernameGists(username, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#getV5UsersUsernameGists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Code&gt;**](Code.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5GistsGistIdCommentsId"></a>
# **patchV5GistsGistIdCommentsId**
> CodeComment patchV5GistsGistIdCommentsId(gistId, id, body, accessToken)

修改代码片段的评论

修改代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
Integer id = 56; // Integer | 评论的ID
String body = "body_example"; // String | 评论内容
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    CodeComment result = apiInstance.patchV5GistsGistIdCommentsId(gistId, id, body, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#patchV5GistsGistIdCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **id** | **Integer**| 评论的ID |
 **body** | **String**| 评论内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CodeComment**](CodeComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5GistsId"></a>
# **patchV5GistsId**
> CodeForksHistory patchV5GistsId(id, accessToken, files, description, _public)

修改代码片段

修改代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
File files = new File("/path/to/file.txt"); // File | Hash形式的代码片段文件名以及文件内容。如: { \"file1.txt\": { \"content\": \"String file contents\" } }
String description = "description_example"; // String | 代码片段描述，1~30个字符
Boolean _public = true; // Boolean | 公开/私有，默认: 私有
try {
    CodeForksHistory result = apiInstance.patchV5GistsId(id, accessToken, files, description, _public);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#patchV5GistsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]
 **files** | **File**| Hash形式的代码片段文件名以及文件内容。如: { \&quot;file1.txt\&quot;: { \&quot;content\&quot;: \&quot;String file contents\&quot; } } | [optional]
 **description** | **String**| 代码片段描述，1~30个字符 | [optional]
 **_public** | **Boolean**| 公开/私有，默认: 私有 | [optional]

### Return type

[**CodeForksHistory**](CodeForksHistory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5Gists"></a>
# **postV5Gists**
> List&lt;CodeForksHistory&gt; postV5Gists(files, description, accessToken, _public)

创建代码片段

创建代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
File files = new File("/path/to/file.txt"); // File | Hash形式的代码片段文件名以及文件内容。如: { \"file1.txt\": { \"content\": \"String file contents\" } }
String description = "description_example"; // String | 代码片段描述，1~30个字符
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean _public = true; // Boolean | 公开/私有，默认: 私有
try {
    List<CodeForksHistory> result = apiInstance.postV5Gists(files, description, accessToken, _public);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#postV5Gists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **files** | **File**| Hash形式的代码片段文件名以及文件内容。如: { \&quot;file1.txt\&quot;: { \&quot;content\&quot;: \&quot;String file contents\&quot; } } |
 **description** | **String**| 代码片段描述，1~30个字符 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **_public** | **Boolean**| 公开/私有，默认: 私有 | [optional]

### Return type

[**List&lt;CodeForksHistory&gt;**](CodeForksHistory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5GistsGistIdComments"></a>
# **postV5GistsGistIdComments**
> CodeComment postV5GistsGistIdComments(gistId, body, accessToken)

增加代码片段的评论

增加代码片段的评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String gistId = "gistId_example"; // String | 代码片段的ID
String body = "body_example"; // String | 评论内容
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    CodeComment result = apiInstance.postV5GistsGistIdComments(gistId, body, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#postV5GistsGistIdComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **String**| 代码片段的ID |
 **body** | **String**| 评论内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CodeComment**](CodeComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5GistsIdForks"></a>
# **postV5GistsIdForks**
> postV5GistsIdForks(id, accessToken)

Fork代码片段

Fork代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.postV5GistsIdForks(id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#postV5GistsIdForks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5GistsIdStar"></a>
# **putV5GistsIdStar**
> putV5GistsIdStar(id, accessToken)

Star代码片段

Star代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GistsApi;


GistsApi apiInstance = new GistsApi();
String id = "id_example"; // String | 代码片段的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.putV5GistsIdStar(id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling GistsApi#putV5GistsIdStar");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 代码片段的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

