
# Contributor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**contributions** | **String** |  |  [optional]



