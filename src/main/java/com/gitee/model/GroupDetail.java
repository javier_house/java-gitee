/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * 更新授权用户所管理的组织资料
 */
@ApiModel(description = "更新授权用户所管理的组织资料")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-01-21T15:12:20.359+08:00")
public class GroupDetail {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("login")
  private String login = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("avatar_url")
  private String avatarUrl = null;

  @SerializedName("repos_url")
  private String reposUrl = null;

  @SerializedName("events_url")
  private String eventsUrl = null;

  @SerializedName("members_url")
  private String membersUrl = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("created_at")
  private String createdAt = null;

  @SerializedName("type")
  private String type = null;

  @SerializedName("location")
  private String location = null;

  @SerializedName("email")
  private String email = null;

  @SerializedName("html_url")
  private String htmlUrl = null;

  @SerializedName("public")
  private String _public = null;

  @SerializedName("enterprise")
  private String enterprise = null;

  @SerializedName("members")
  private String members = null;

  @SerializedName("public_repos")
  private String publicRepos = null;

  @SerializedName("private_repos")
  private String privateRepos = null;

  @SerializedName("owner")
  private String owner = null;

  public GroupDetail id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public GroupDetail login(String login) {
    this.login = login;
    return this;
  }

   /**
   * Get login
   * @return login
  **/
  @ApiModelProperty(value = "")
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public GroupDetail url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public GroupDetail avatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
    return this;
  }

   /**
   * Get avatarUrl
   * @return avatarUrl
  **/
  @ApiModelProperty(value = "")
  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public GroupDetail reposUrl(String reposUrl) {
    this.reposUrl = reposUrl;
    return this;
  }

   /**
   * Get reposUrl
   * @return reposUrl
  **/
  @ApiModelProperty(value = "")
  public String getReposUrl() {
    return reposUrl;
  }

  public void setReposUrl(String reposUrl) {
    this.reposUrl = reposUrl;
  }

  public GroupDetail eventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
    return this;
  }

   /**
   * Get eventsUrl
   * @return eventsUrl
  **/
  @ApiModelProperty(value = "")
  public String getEventsUrl() {
    return eventsUrl;
  }

  public void setEventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
  }

  public GroupDetail membersUrl(String membersUrl) {
    this.membersUrl = membersUrl;
    return this;
  }

   /**
   * Get membersUrl
   * @return membersUrl
  **/
  @ApiModelProperty(value = "")
  public String getMembersUrl() {
    return membersUrl;
  }

  public void setMembersUrl(String membersUrl) {
    this.membersUrl = membersUrl;
  }

  public GroupDetail description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public GroupDetail name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public GroupDetail createdAt(String createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  @ApiModelProperty(value = "")
  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public GroupDetail type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public GroupDetail location(String location) {
    this.location = location;
    return this;
  }

   /**
   * Get location
   * @return location
  **/
  @ApiModelProperty(value = "")
  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public GroupDetail email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public GroupDetail htmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
    return this;
  }

   /**
   * Get htmlUrl
   * @return htmlUrl
  **/
  @ApiModelProperty(value = "")
  public String getHtmlUrl() {
    return htmlUrl;
  }

  public void setHtmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
  }

  public GroupDetail _public(String _public) {
    this._public = _public;
    return this;
  }

   /**
   * Get _public
   * @return _public
  **/
  @ApiModelProperty(value = "")
  public String getPublic() {
    return _public;
  }

  public void setPublic(String _public) {
    this._public = _public;
  }

  public GroupDetail enterprise(String enterprise) {
    this.enterprise = enterprise;
    return this;
  }

   /**
   * Get enterprise
   * @return enterprise
  **/
  @ApiModelProperty(value = "")
  public String getEnterprise() {
    return enterprise;
  }

  public void setEnterprise(String enterprise) {
    this.enterprise = enterprise;
  }

  public GroupDetail members(String members) {
    this.members = members;
    return this;
  }

   /**
   * Get members
   * @return members
  **/
  @ApiModelProperty(value = "")
  public String getMembers() {
    return members;
  }

  public void setMembers(String members) {
    this.members = members;
  }

  public GroupDetail publicRepos(String publicRepos) {
    this.publicRepos = publicRepos;
    return this;
  }

   /**
   * Get publicRepos
   * @return publicRepos
  **/
  @ApiModelProperty(value = "")
  public String getPublicRepos() {
    return publicRepos;
  }

  public void setPublicRepos(String publicRepos) {
    this.publicRepos = publicRepos;
  }

  public GroupDetail privateRepos(String privateRepos) {
    this.privateRepos = privateRepos;
    return this;
  }

   /**
   * Get privateRepos
   * @return privateRepos
  **/
  @ApiModelProperty(value = "")
  public String getPrivateRepos() {
    return privateRepos;
  }

  public void setPrivateRepos(String privateRepos) {
    this.privateRepos = privateRepos;
  }

  public GroupDetail owner(String owner) {
    this.owner = owner;
    return this;
  }

   /**
   * Get owner
   * @return owner
  **/
  @ApiModelProperty(value = "")
  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GroupDetail groupDetail = (GroupDetail) o;
    return Objects.equals(this.id, groupDetail.id) &&
        Objects.equals(this.login, groupDetail.login) &&
        Objects.equals(this.url, groupDetail.url) &&
        Objects.equals(this.avatarUrl, groupDetail.avatarUrl) &&
        Objects.equals(this.reposUrl, groupDetail.reposUrl) &&
        Objects.equals(this.eventsUrl, groupDetail.eventsUrl) &&
        Objects.equals(this.membersUrl, groupDetail.membersUrl) &&
        Objects.equals(this.description, groupDetail.description) &&
        Objects.equals(this.name, groupDetail.name) &&
        Objects.equals(this.createdAt, groupDetail.createdAt) &&
        Objects.equals(this.type, groupDetail.type) &&
        Objects.equals(this.location, groupDetail.location) &&
        Objects.equals(this.email, groupDetail.email) &&
        Objects.equals(this.htmlUrl, groupDetail.htmlUrl) &&
        Objects.equals(this._public, groupDetail._public) &&
        Objects.equals(this.enterprise, groupDetail.enterprise) &&
        Objects.equals(this.members, groupDetail.members) &&
        Objects.equals(this.publicRepos, groupDetail.publicRepos) &&
        Objects.equals(this.privateRepos, groupDetail.privateRepos) &&
        Objects.equals(this.owner, groupDetail.owner);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, login, url, avatarUrl, reposUrl, eventsUrl, membersUrl, description, name, createdAt, type, location, email, htmlUrl, _public, enterprise, members, publicRepos, privateRepos, owner);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GroupDetail {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    login: ").append(toIndentedString(login)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    avatarUrl: ").append(toIndentedString(avatarUrl)).append("\n");
    sb.append("    reposUrl: ").append(toIndentedString(reposUrl)).append("\n");
    sb.append("    eventsUrl: ").append(toIndentedString(eventsUrl)).append("\n");
    sb.append("    membersUrl: ").append(toIndentedString(membersUrl)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
    sb.append("    _public: ").append(toIndentedString(_public)).append("\n");
    sb.append("    enterprise: ").append(toIndentedString(enterprise)).append("\n");
    sb.append("    members: ").append(toIndentedString(members)).append("\n");
    sb.append("    publicRepos: ").append(toIndentedString(publicRepos)).append("\n");
    sb.append("    privateRepos: ").append(toIndentedString(privateRepos)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

