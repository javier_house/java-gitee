/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.api;

import com.gitee.ApiException;
import com.gitee.model.Label;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for LabelsApi
 */
@Ignore
public class LabelsApiTest {

    private final LabelsApi api = new LabelsApi();

    
    /**
     * 删除Issue所有标签
     *
     * 删除Issue所有标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteV5ReposOwnerRepoIssuesNumberLabelsTest() throws ApiException {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        api.deleteV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);

        // TODO: test validations
    }
    
    /**
     * 删除Issue标签
     *
     * 删除Issue标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteV5ReposOwnerRepoIssuesNumberLabelsNameTest() throws ApiException {
        String owner = null;
        String repo = null;
        String number = null;
        String name = null;
        String accessToken = null;
        api.deleteV5ReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name, accessToken);

        // TODO: test validations
    }
    
    /**
     * 删除一个仓库任务标签
     *
     * 删除一个仓库任务标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteV5ReposOwnerRepoLabelsNameTest() throws ApiException {
        String owner = null;
        String repo = null;
        String name = null;
        String accessToken = null;
        api.deleteV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取企业所有标签
     *
     * 获取企业所有标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5EnterprisesEnterpriseLabelsTest() throws ApiException {
        String enterprise = null;
        String accessToken = null;
        List<Label> response = api.getV5EnterprisesEnterpriseLabels(enterprise, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取企业某个标签
     *
     * 获取企业某个标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5EnterprisesEnterpriseLabelsNameTest() throws ApiException {
        String enterprise = null;
        String name = null;
        String accessToken = null;
        Label response = api.getV5EnterprisesEnterpriseLabelsName(enterprise, name, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取仓库任务的所有标签
     *
     * 获取仓库任务的所有标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoIssuesNumberLabelsTest() throws ApiException {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        List<Label> response = api.getV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取仓库所有任务标签
     *
     * 获取仓库所有任务标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoLabelsTest() throws ApiException {
        String owner = null;
        String repo = null;
        String accessToken = null;
        List<Label> response = api.getV5ReposOwnerRepoLabels(owner, repo, accessToken);

        // TODO: test validations
    }
    
    /**
     * 根据标签名称获取单个标签
     *
     * 根据标签名称获取单个标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoLabelsNameTest() throws ApiException {
        String owner = null;
        String repo = null;
        String name = null;
        String accessToken = null;
        Label response = api.getV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken);

        // TODO: test validations
    }
    
    /**
     * 更新一个仓库任务标签
     *
     * 更新一个仓库任务标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void patchV5ReposOwnerRepoLabelsOriginalNameTest() throws ApiException {
        String owner = null;
        String repo = null;
        String originalName = null;
        String accessToken = null;
        String name = null;
        String color = null;
        Label response = api.patchV5ReposOwnerRepoLabelsOriginalName(owner, repo, originalName, accessToken, name, color);

        // TODO: test validations
    }
    
    /**
     * 创建Issue标签
     *
     * 创建Issue标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void postV5ReposOwnerRepoIssuesNumberLabelsTest() throws ApiException {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        List<String> body = null;
        Label response = api.postV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken, body);

        // TODO: test validations
    }
    
    /**
     * 创建仓库任务标签
     *
     * 创建仓库任务标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void postV5ReposOwnerRepoLabelsTest() throws ApiException {
        String owner = null;
        String repo = null;
        String name = null;
        String color = null;
        String accessToken = null;
        Label response = api.postV5ReposOwnerRepoLabels(owner, repo, name, color, accessToken);

        // TODO: test validations
    }
    
    /**
     * 替换Issue所有标签
     *
     * 替换Issue所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void putV5ReposOwnerRepoIssuesNumberLabelsTest() throws ApiException {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        List<String> body = null;
        Label response = api.putV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken, body);

        // TODO: test validations
    }
    
}
